const path = require('path')

//Servidor
const express = require('express')
const app = express()

//Middlewares
const bodyParser = require('body-parser')
const cors = require('cors')

//Middlewares
app.use(bodyParser.json()) //Peticiones JSON
app.use(bodyParser.urlencoded({extended: true}))
app.use(express.static(path.join(__dirname, "public")))
app.use(cors())

//Rutas
const main_routes = require('./routes')
app.use('/app', main_routes)
const user_routes = require('./routes/usuarios')
app.use('/users', user_routes)

module.exports = app
