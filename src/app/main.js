//config
import config from '../config'

//Modulos
import Vue from 'vue'
import router from './router'
import VueAxios from 'vue-axios'
import axios from 'axios'
//Sockets
import VueSocketio from 'vue-socket.io';
Vue.use(VueSocketio, 'http://localhost:8080');

//Vuex
import {store} from './store'

//Components
import App from './App.vue'

//Middlewares
Vue.use(VueAxios, axios)

Vue.config.productionTip = false
let app;

//Firebase
import firebase from 'firebase'
const firebase_config = config.firebase_config
firebase.initializeApp(firebase_config);
//Cambios en el Login
firebase.auth().onAuthStateChanged(function(user) {
  if(!app){
      app = new Vue({
      el: '#app',
      render: h => h(App),
      store: store,
      router: router
    })
  }
  if (user) {
    // User is signed in.
    var uid = user.uid;
    var email = user.email;
    var displayName = user.displayName;

    console.log('Usuario conectado', displayName)
    store.commit('setUser', displayName)
  } else {

    return console.log('Usuario desconectado')
  }
})
