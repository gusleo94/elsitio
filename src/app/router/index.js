import Vue from 'vue'
import Router from 'vue-router'
import firebase from 'firebase'

//Components
import Chat from '../components/Chat'
import Home from '../components/Home'
import Perfil from '../components/Perfil'
import Register from '../components/Register'
import Login from '../components/Login'

Vue.use(Router)

let router = new Router({
  routes: [
    {
      path: '*',
      redirect: '/'
    },
    {
      name: 'Chat',
      path: '/chat',
      component: Chat,
      meta: {
        requiresAuth: true
      }
    },
    {
      name: 'Home',
      path: '/home',
      component: Home,
      meta: {
        requiresAuth: true
      }
    },
    {
      name: 'Perfil',
      path: '/perfil',
      component: Perfil,
      meta: {
        requiresAuth: true
      }
    },
    {
      name: 'Register',
      path: '/register',
      component: Register
    },
    {
      name: 'Login',
      path: '/',
      component: Login
    }
  ]
})

router.beforeEach((to, from, next) => {
  let currentUser = firebase.auth().currentUser
  let requiresAuth = to.matched.some(record => record.meta.requiresAuth)

  if(requiresAuth && !currentUser) next('Login')
  else if(!requiresAuth && currentUser) next('Chat')
  else next()
})

export default router
