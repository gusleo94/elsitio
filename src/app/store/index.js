import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    auth: false,
    displayName: ""
  },
  getters: {

  },
  mutations: {
    changeAuth: state =>{
      state.auth = !state.auth
    },
    setUser(state, displayName){
      state.displayName = displayName
    }
  },
  actions: {
    setUser(context, displayName){
      context.commit('setUser', displayName)
    }
  }
})
