const Chat_mgs = require('../models/chat_mgs')
const config = require('../config')

function createMessage(mgs) {
  let message = new Chat_mgs({
    index: mgs.index,
    displayName: mgs.user,
    mensaje: mgs.mensaje
  })
  message.save(err =>{
    if(!err){
      config.mgs_counter += 1
      Chat_mgs.findOneAndRemove({index: config.mgs_counter - config.max_mgs}, (err, item) =>{
        if(err) return console.log(`Ha ocurrido un error: ${err}`)
      })
    }
  })
}

function searchMessages(){
  return new Promise(function (resolve, reject) {
    Chat_mgs.find({}, (err, items) => {
      if(err) return reject(err)
      resolve(items)
    })
  })
}

function translateMessages(items) {
  return new Promise(function(resolve, reject) {
    for(i=0; i<= items.length; i++){
      let mensaje ={
        user: items[i].displayName,
        mgs: items[i].mensaje
      }
      resolve(mensaje)
    }
  })

}

module.exports = {
  createMessage,
  searchMessages,
  translateMessages
}
