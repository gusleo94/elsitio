//CutomMiddlewares
const chat_mgs = require('./chatMessages')
const config = require('../config')

module.exports = function (io) {
  io.on('connection', function(socket){
  console.log('a user connected')

  socket.on('chat_message', (mgs) => {
    chat_mgs.createMessage({
      index: config.mgs_counter,
      user: mgs.user,
      mensaje: mgs.message
    })
    io.emit('chat_message', mgs)
  })

  /*socket.on('mgs_request', () =>{
    chat_mgs.searchMessages()
    .then(items =>{
      chat_mgs.translateMessages(items)
      .then((item) =>{
        console.log('Estas en el then', item)
        io.emit('old_mgs', item)
      })
    })
    .catch(err => console.log(`Hubo un error en mgs_request: ${err}`))
  })*/
  })

}
