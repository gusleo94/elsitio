//Firebase
var admin = require("firebase-admin");
var serviceAccount = require("../config/elsitio-289bd-firebase-adminsdk-1ldf3-25c0b45dcf.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://elsitio-289bd.firebaseio.com"
});

module.exports = {
  createUser: function (res, user) {
    admin.auth().createUser({
      email: user.email,
      password: user.password,
      displayName: user.displayName
    })
    .then(function(userRecord) {
      res.status(200).redirect('/')
      console.log("Successfully created new user:", userRecord.displayName)
    })
    .catch(function(error) {
      console.log("Error creating new user:", error)
    })
  },
  updateUser: function () {

  }
}
