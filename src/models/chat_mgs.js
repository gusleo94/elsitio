const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Chat_mgs = new Schema({
  index: {type: Number},
  displayName: {type : String},
  mensaje: {type : String}
},{
  collection: 'Mensajes_Chat'
})

module.exports = mongoose.model('Chat_mgs', Chat_mgs)
