const mongoose = require('mongoose')
const Schema = mongoose.Schema

const User = new Schema({
  username: {type : String},
  password: {type: String},
  displayName: {type: String}
},{
  collection: 'esUsuarios'
})

module.exports = mongoose.model('User', User)
