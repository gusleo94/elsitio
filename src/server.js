//Modulos
const config = require('./config')

//Express
const app  = require('./app')

//Sockets
const server = require("http").Server(app)
const io = require("socket.io")(server)
require('./controllers/sockets')(io)

//Variables de configuracion
const port = config.port
const db = config.db

//DB
const mongoose = require('mongoose')
mongoose.Promise = global.Promise
mongoose.connect(db, {
  useMongoClient: true
}).then(() => {console.log('Conexion con DB')})
  .catch((err) =>{console.log(`Error al conectar DB: ${err}`)})



//Server
server.listen(port, () => {
  console.log(`Servidor funcionando en : ${port}`)
})

module.exports = server;
